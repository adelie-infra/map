============================================
 README for Adélie Linux Infrastructure Map
============================================
:Authors:
  * **A. Wilcox**, Distro Lead, principal mapper
  * **Adélie Linux Infrastructure Team**, teaming the infrastructure
:Status:
  Production, hopefully
:Copyright:
  © 2019 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux infrastructure map, and related
tooling.


Licenses
`````````
The tooling around our infrastructure map is licensed under the NCSA license.
The map itself is considered simple data and non-copyrightable.  If this does
not apply in your jurisdiction, it is hereby released into the public domain,
with no rights retained to it.  Feel free to use it as a basis for your own
infrastructure, corporate, private, or public.




Contents
========

``map.xml``: The Map
````````````````````
Self-explanatory.


``some tools``: Tooling
```````````````````````
When we write some tools, write about it here!
