<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<html>
	<head>
		<title>Adélie Infrastructure - All Machines</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/core/base.min.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/screen/typography.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/forms/gray-theme.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/navigation/hlist.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/font/font-amal.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/adelie.css" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/adelie.desktop.css" media="screen and (min-width: 761px)" type="text/css"/>
		<link rel="stylesheet" href="//static.adelielinux.org/css/adelie.mobile.css" media="screen and (max-width: 760px)" type="text/css"/>
	</head>
	<body>
		<div id="main" role="main">
			<div class="ym-wrapper center">
				<h1>All Adélie Infrastructure machines</h1>
				<table border="1">
				<thead>
					<tr><th>Type / Role</th><th>Name</th><th>Canonical DNS</th><th>Arch</th><th>OS</th><th>RAM (MB)</th><th>Disk (MB)</th><th>Notes</th></tr>
				</thead>
				<tbody>
				<xsl:for-each select="map/node">
					<tr>
						<td><xsl:value-of select="@type" /><xsl:if test="@type = 'vm'"> (on <xsl:value-of select="host" />)</xsl:if><xsl:if test="@role"><![CDATA[ — ]]><xsl:value-of select="@role" /></xsl:if></td>
						<td><xsl:value-of select="name" /></td>
						<td><xsl:value-of select="dns" /></td>
						<td><xsl:value-of select="arch" /></td>
						<td><xsl:value-of select="os" /></td>
						<td><xsl:value-of select="ram" /></td>
						<td><xsl:for-each select="disk"><p><xsl:value-of select="@device" />: <xsl:value-of select="." /></p></xsl:for-each></td>
						<td><xsl:value-of select="notes" /></td>
					</tr>
				</xsl:for-each>
				</tbody>
				</table>
			</div>
		</div>
	</body>
	</html>
	</xsl:template>
</xsl:stylesheet>
